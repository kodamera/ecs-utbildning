(function($) {

  Drupal.behaviors.exampleGet = {
    attach: function (context, settings) {
      $.get('/admin/examples/javascript/json', function(result) {
        console.log(result);
      });
    }
  }

  Drupal.behaviors.exampleSettings = {
    attach: function (context, settings) {
      // console.log(settings.custom_javascript);
    }
  }

})(jQuery);
