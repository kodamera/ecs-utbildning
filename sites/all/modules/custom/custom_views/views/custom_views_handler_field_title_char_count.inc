<?php

class custom_views_handler_field_title_char_count extends views_handler_field_numeric {

  /**
   * query() is part of building the query for the views.
   * In this case we want to add a field that counts the title length.
   */
  function query() {
    // Make sure the table is available, and get the alias.
    $table_alias = $this->ensure_my_table();

    // Count the title field with a LENGTH() expression.
    $field = "$this->table_alias.title";
    $this->query->add_field(NULL, "LENGTH($field)", $table_alias . '_character_count');
  }

  /**
   * render() is responsible for the display of the field.
   */
  function render($data) {
    $table_alias = $this->ensure_my_table();
    return $data->{$table_alias . '_character_count'};
  }

}
