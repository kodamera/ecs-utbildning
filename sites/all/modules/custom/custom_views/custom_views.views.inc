<?php

/**
 * Implements hook_views_handlers().
 */
function custom_views_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'custom_views') . '/views',
    ),
    'handlers' => array(
      'custom_views_handler_field_title_char_count' => array(
        'parent' => 'views_handler_field_numeric',
      ),
    ),
  );
}

/**
 * Implements hook_views_data().
 */
function custom_views_views_data() {
  $data = array();

  $data['node']['node_title_character_count'] = array(
    'title' => t('Node title character count'),
    'help' => t('Displays the number of characters of the node title.'),
    'field' => array(
      'handler' => 'custom_views_handler_field_title_char_count',
    ),
  );

  return $data;
}
