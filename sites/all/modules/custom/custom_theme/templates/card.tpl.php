<div class="card">
  <?php if ($image_url): ?>
    <img class="card-img-top" src="<?php print $image_url; ?>" alt="<?php print $image_alt; ?>">
  <?php endif; ?>
  <div class="card-body">
    <?php if ($title): ?>
      <h4 class="card-title"><?php print $title; ?></h4>
    <?php endif; ?>
    <?php if ($text): ?>
      <div class="card-text">
        <?php print $text; ?>
      </div>
    <?php endif; ?>
    <?php if ($link): ?>
      <?php print $link; ?>
    <?php endif; ?>
  </div>
</div>
