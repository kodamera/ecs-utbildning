<?php

/**
 * @file
 * Declares our migrations.
 */

/**
 * Implements hook_migrate_api().
 */
function custom_migrate_migrate_api() {
  $api = array(
    'api' => 2,
    'groups' => array(
      'custom' => array(
        'title' => t('Custom Migrations'),
      ),
    ),
    'migrations' => array(
      'CustomBlogposts' => array(
        'class_name' => 'CustomBlogpostsMigration',
        'group_name' => 'custom',
      ),
    ),
  );
  return $api;
}
