<?php

class CustomBlogpostsMigration extends Migration {
  /**
   * Path of the CSV file.
   * @var string
   */
  public $file_path;

  public function __construct($arguments) {
    parent::__construct($arguments);
    $this->description = t('Import blog posts from CSV file.');
    $file_path = drupal_get_path('module', 'custom_migrate') . '/source/blogposts.csv';
    // $file_path = 'http://www.kodamera.se/sites/default/files/blogposts.csv';
    $this->file_path = $file_path;

    // Create a MigrateSource object, which manages retrieving the input data.
    $source_options = array(
      'header_rows' => TRUE,
    );
    $this->source = new MigrateSourceCSV($this->file_path, array(), $source_options);

    // Create a MigrateDestination object, which manages what data we're
    // creating in the migration.
    $this->destination = new MigrateDestinationNode('blog');

    // Create a map object for tracking the relationships between source rows
    // and destination item.
    $this->map = new MigrateSQLMap(
      // Machine name of the Migration. Used to name the map table.
      $this->machineName,
      // The database schema of the source key. We define it as an unsigned
      // integer.
      array(
        'blogpost_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => 'ID of blogpost',
        ),
      ),
      // The database schema of the destination key.
      MigrateDestinationNode::getKeySchema()
    );

    // Map destination fields to a source fields.
    $this->addSimpleMappings(array(
      'body',
    ));
    // $this->addFieldMapping('field_image', 'custom-image');
    $this->addFieldMapping('title', 'custom_title');
  }

  public function prepareRow($row) {
    $row->custom_title = 'Custom title';
  }

  public function prepare()
  {

  }

  public function complete()
  {

  }

}
