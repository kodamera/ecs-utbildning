<?php
/**
 * @file
 * view_blogposts.features.inc
 */

/**
 * Implements hook_views_api().
 */
function view_blogposts_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
